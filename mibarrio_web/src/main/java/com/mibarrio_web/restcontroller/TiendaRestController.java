package com.mibarrio_web.restcontroller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mibarrio_web.model.Tienda;
import com.mibarrio_web.model.Usuario;
import com.mibarrio_web.repository.TiendaRepository;
import com.mibarrio_web.repository.UsuarioRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/tienda")
public class TiendaRestController {

	@Autowired
	private TiendaRepository tiendaRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@GetMapping
	public ResponseEntity<List<Tienda>> listarTiendas() {
		return ResponseEntity.ok(tiendaRepository.findCurrentUserTiendas());
	}

	@PostMapping
	public ResponseEntity<Tienda> guardarTienda(@Valid @RequestBody Tienda tienda) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(tienda.getUsuario().getId());

		if (!usuarioOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
		}

		tienda.setUsuario(usuarioOptional.get());
		Tienda tiendaGuardada = tiendaRepository.save(tienda);
		URI ubicacion = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(tiendaGuardada.getId()).toUri();
		return ResponseEntity.created(ubicacion).body(tiendaGuardada);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Tienda> actualizarTienda(@Valid @RequestBody Tienda tienda, @PathVariable Integer id) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findById(tienda.getUsuario().getId());

		if (!usuarioOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
		}

		Optional<Tienda> tiendaOptional = tiendaRepository.findById(id);

		if (!tiendaOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
		}

		tienda.setUsuario(usuarioOptional.get());
		tienda.setId(tiendaOptional.get().getId());
		tiendaRepository.save(tienda);
		return ResponseEntity.noContent().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Tienda> eliminarTienda(@PathVariable Integer id) {
		Optional<Tienda> tiendaOptional = tiendaRepository.findById(id);

		if (!tiendaOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
		}

		tiendaRepository.delete(tiendaOptional.get());
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Tienda> obtenerTiendaPorId(@PathVariable Integer id) {
		Optional<Tienda> tiendaOptional = tiendaRepository.findById(id);

		if (!tiendaOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
		}
		return ResponseEntity.ok(tiendaOptional.get());
	}
	
}
