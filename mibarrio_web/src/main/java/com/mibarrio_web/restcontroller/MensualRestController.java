package com.mibarrio_web.restcontroller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mibarrio_web.model.Mensual;
import com.mibarrio_web.model.Tienda;
import com.mibarrio_web.repository.MensualRepository;
import com.mibarrio_web.repository.TiendaRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/mensual")
public class MensualRestController {

	@Autowired
	private MensualRepository mensualRepository;
	
	@Autowired
	private TiendaRepository tiendaRepository;
	

	@GetMapping
	public ResponseEntity<List<Mensual>> listarMensual(){
		return ResponseEntity.ok(mensualRepository.findCurrentUserMensual());
	}
	
	@PostMapping
	public ResponseEntity<Mensual> guardarMensual(@Valid @RequestBody Mensual mensual) {
		Optional<Tienda> tiendaOptional = tiendaRepository.findById(mensual.getTienda().getId());
		
		if (!tiendaOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		mensual.setTienda(tiendaOptional.get());
		Mensual mensualGuardado = mensualRepository.save(mensual);
		URI ubicacion = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(mensualGuardado.getId()).toUri();
		return ResponseEntity.created(ubicacion).body(mensualGuardado);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Mensual> actualizarMensual(@Valid @RequestBody Mensual mensual, @PathVariable Integer id) {
		Optional<Tienda> tiendaOptional = tiendaRepository.findById(mensual.getTienda().getId());
		
		if (!tiendaOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		Optional<Mensual> mensualOptional = mensualRepository.findById(id);
		
		if (!mensualOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		mensual.setTienda(tiendaOptional.get());
		mensual.setId(mensualOptional.get().getId());
		mensualRepository.save(mensual);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Mensual> eliminarMensual(@PathVariable Integer id){
		Optional<Mensual> mensualOptional = mensualRepository.findById(id);
		
		if (!mensualOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		mensualRepository.delete(mensualOptional.get());
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Mensual> obtenerMensualPorId(@PathVariable Integer id){
		Optional<Mensual> mensualOptional = mensualRepository.findById(id);
		
		if (!mensualOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		return ResponseEntity.ok(mensualOptional.get());
	}
}
