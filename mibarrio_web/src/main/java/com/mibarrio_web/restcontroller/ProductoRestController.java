package com.mibarrio_web.restcontroller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mibarrio_web.model.Producto;
import com.mibarrio_web.model.Tienda;
import com.mibarrio_web.repository.ProductoRepository;
import com.mibarrio_web.repository.TiendaRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/producto")
public class ProductoRestController {
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private TiendaRepository tiendaRepository;

	@GetMapping
	public ResponseEntity<List<Producto>> listarProductos(){
		return ResponseEntity.ok(productoRepository.findCurrentUserProductos());
	}
	
	@PostMapping
	public ResponseEntity<Producto> guardarProductos(@Valid @RequestBody Producto producto) {
		Optional<Tienda> tiendaOptional = tiendaRepository.findById(producto.getTienda().getId());
		
		if (!tiendaOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		producto.setTienda(tiendaOptional.get());
		Producto productoGuardado = productoRepository.save(producto);
		URI ubicacion = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(productoGuardado.getId()).toUri();
		return ResponseEntity.created(ubicacion).body(productoGuardado);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Producto> actualizarProducto(@Valid @RequestBody Producto producto, @PathVariable Integer id) {
		Optional<Tienda> tiendaOptional = tiendaRepository.findById(producto.getTienda().getId());
		
		if (!tiendaOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		Optional<Producto> productoOptional = productoRepository.findById(id);
		
		if (!productoOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		producto.setTienda(tiendaOptional.get());
		producto.setId(productoOptional.get().getId());
		productoRepository.save(producto);
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Producto> eliminarProducto(@PathVariable Integer id){
		Optional<Producto> productoOptional = productoRepository.findById(id);
		
		if (!productoOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		
		productoRepository.delete(productoOptional.get());
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable Integer id){
		Optional<Producto> productoOptional = productoRepository.findById(id);
		
		if (!productoOptional.isPresent()) {
			return ResponseEntity.unprocessableEntity().build();
			}
		return ResponseEntity.ok(productoOptional.get());
	}
}
