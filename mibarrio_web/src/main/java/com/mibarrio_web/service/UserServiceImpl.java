package com.mibarrio_web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mibarrio_web.dto.UserDto;
import com.mibarrio_web.model.Usuario;
import com.mibarrio_web.repository.UsuarioRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Bean
	public BCryptPasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public Usuario save(UserDto userDto) {
		Usuario usuario = new Usuario(userDto.getUsername(), userDto.getEmail(),
				encoder().encode(userDto.getPassword()));
		return usuarioRepository.save(usuario);
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findUserByEmail(email);
		if (usuario == null) {
			throw new UsernameNotFoundException("Usuario o password inválidos");
		}

		List<GrantedAuthority> roles = new ArrayList<>();
		roles.add(new SimpleGrantedAuthority("USER"));

		UserDetails userDet = new User(usuario.getUsername(), usuario.getPassword(), roles);
		return userDet;
	}
}
