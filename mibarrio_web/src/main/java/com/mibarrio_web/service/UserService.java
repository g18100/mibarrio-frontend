package com.mibarrio_web.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.mibarrio_web.dto.UserDto;
import com.mibarrio_web.model.Usuario;

public interface UserService extends UserDetailsService{
	Usuario save(UserDto userDto);
}
