package com.mibarrio_web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mibarrio_web.model.Producto;
import com.mibarrio_web.model.Tienda;
import com.mibarrio_web.repository.ProductoRepository;
import com.mibarrio_web.repository.TiendaRepository;

@Controller
@CrossOrigin("*")
public class ProductoController {

	@Autowired
	private ProductoRepository productoRepository;

	@Autowired
	private TiendaRepository tiendaRepository;

	@RequestMapping("/productos")
	public String listarProductos(Model model, @Param("kw") String kw) {
		if (kw != null) {
			List<Producto> listaProductos = productoRepository.findCurrentUserProductosSearch(kw);
			model.addAttribute("listaProductos", listaProductos);
			model.addAttribute("kw", kw);
		} else {
			List<Producto> listaProductos = productoRepository.findCurrentUserProductos();
			model.addAttribute("listaProductos", listaProductos);
		}
		return "productos";
	}

	@RequestMapping(value = "/productos/perecedero", method = RequestMethod.GET)
	public String listarPerecedero(Model model) {
		List<Producto> listaProductos = productoRepository.findCurrentUserProductosPerecedero();
		model.addAttribute("listaProductos", listaProductos);
		return "productos";
	}

	@RequestMapping(value = "/productos/stock", method = RequestMethod.GET)
	public String listarStock(Model model) {
		List<Producto> listaProductos = productoRepository.findCurrentUserProductosStock();
		model.addAttribute("listaProductos", listaProductos);
		return "productos";
	}

	@RequestMapping(value = "/productos/ventas", method = RequestMethod.GET)
	public String listarVentas(Model model) {
		List<Producto> listaProductos = productoRepository.findCurrentUserProductosVentas();
		model.addAttribute("listaProductos", listaProductos);
		return "productos";
	}

	@RequestMapping(value = "productos/nuevo", method = RequestMethod.GET)
	public String mostrarFormularioProductos(Model model) {
		List<Tienda> listaTiendas = tiendaRepository.findCurrentUserTiendas();
		model.addAttribute("listaTiendas", listaTiendas);
		model.addAttribute("producto", new Producto());
		return "form_producto";
	}

	@RequestMapping(value = "productos/guardar", method = RequestMethod.POST)
	public String guardarProducto(@ModelAttribute("producto") Producto producto) {
		productoRepository.save(producto);
		return "redirect:/productos";
	}

	@RequestMapping("/productos/editar/{id}")
	public String mostrarFormularioModificarProducto(@PathVariable("id") Integer id, Model model) {
		Producto producto = productoRepository.findById(id).get();
		model.addAttribute("producto", producto);
		List<Tienda> listaTiendas = tiendaRepository.findCurrentUserTiendas();
		model.addAttribute("listaTiendas", listaTiendas);
		return "form_producto";
	}

	@RequestMapping("/productos/venta/{id}")
	public String mostrarFormularioRegVenta(@PathVariable("id") Integer id, Model model) {
		Producto producto = productoRepository.findById(id).get();
		model.addAttribute("producto", producto);
		List<Tienda> listaTiendas = tiendaRepository.findCurrentUserTiendas();
		model.addAttribute("listaTiendas", listaTiendas);
		return "form_reg_venta";
	}

	// Eliminación de productos

	/*
	 * @RequestMapping("/productos/eliminar/{id}") public String
	 * eliminarProducto(@PathVariable("id") Integer id) {
	 * productoRepository.deleteById(id); return "redirect:/productos"; }
	 */
}
