package com.mibarrio_web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mibarrio_web.model.Tienda;
import com.mibarrio_web.model.Usuario;
import com.mibarrio_web.repository.TiendaRepository;
import com.mibarrio_web.repository.UsuarioRepository;

@Controller
@CrossOrigin("*")
public class TiendaController {

	@Autowired
	private TiendaRepository tiendaRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@RequestMapping("/")
	public String listarTiendas(Model model) {
		List<Tienda> listaTiendas = tiendaRepository.findCurrentUserTiendas();
		model.addAttribute("listaTiendas", listaTiendas);
		return "index";
	}

	@RequestMapping(value = "tiendas/nuevo", method = RequestMethod.GET)
	public String mostrarFormularioTienda(Model model) {
		List<Usuario> listaUsuarios = usuarioRepository.findCurrentUser();
		model.addAttribute("tienda", new Tienda());
		model.addAttribute("listaUsuarios", listaUsuarios);
		return "form_tienda";
	}

	@RequestMapping(value = "tiendas/guardar", method = RequestMethod.POST)
	public String guardarTienda(@ModelAttribute("tienda") Tienda tienda) {
		tiendaRepository.save(tienda);
		return "redirect:/";
	}

	@RequestMapping("/tiendas/editar/{id}")
	public String mostrarFormularioModificarTienda(@PathVariable("id") Integer id, Model model) {
		Tienda tienda = tiendaRepository.findById(id).get();
		model.addAttribute("tienda", tienda);
		List<Usuario> listaUsuarios = usuarioRepository.findCurrentUser();
		model.addAttribute("listaUsuarios", listaUsuarios);
		return "form_tienda";
	}

	@RequestMapping("/tiendas/eliminar/{id}")
	public String eliminarTienda(@PathVariable("id") Integer id) {
		tiendaRepository.deleteById(id);
		return "redirect:/";
	}
}
