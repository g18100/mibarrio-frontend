package com.mibarrio_web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mibarrio_web.dto.UserDto;
import com.mibarrio_web.service.UserService;

@Controller
@CrossOrigin("*")
@RequestMapping("/form_user")
public class RegUserController {

	@Autowired
	private UserService userService;

	@ModelAttribute("reg_user")
	public UserDto retornarNuevoUserReg() {
		return new UserDto();
	}

	@GetMapping
	public String mostrarFormularioUserReg() {
		return "form_user";
	}

	@PostMapping
	public String registrarUsuario(@ModelAttribute("reg_user") UserDto userDto) {
		try {
			userService.save(userDto);
			return "redirect:/form_user?exito";
		} catch (Exception e) {
			return "redirect:/form_user?error";
		}
	}
}
