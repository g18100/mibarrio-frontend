package com.mibarrio_web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mibarrio_web.model.Mensual;
import com.mibarrio_web.model.Tienda;
import com.mibarrio_web.repository.MensualRepository;
import com.mibarrio_web.repository.TiendaRepository;

@Controller
@CrossOrigin("*")
public class MensualController {

	@Autowired
	private MensualRepository mensualRepository;

	@Autowired
	private TiendaRepository tiendaRepository;

	@RequestMapping("/mensual")
	public String listarMensual(Model model, @Param("kw") String kw) {
		if (kw != null) {
			List<Mensual> listaMensual = mensualRepository.findCurrentUserMensualSearch(kw);
			model.addAttribute("listaMensual", listaMensual);
			model.addAttribute("kw", kw);
		} else {
			List<Mensual> listaMensual = mensualRepository.findCurrentUserMensual();
			model.addAttribute("listaMensual", listaMensual);
		}
		return "mensual";
	}

	@RequestMapping(value = "/mensual/pagos", method = RequestMethod.GET)
	public String listarFechaPago(Model model) {
		List<Mensual> listaMensual = mensualRepository.findCurrentUserMensualFecha();
		model.addAttribute("listaMensual", listaMensual);
		return "mensual";
	}

	@RequestMapping(value = "mensual/nuevo", method = RequestMethod.GET)
	public String mostrarFormularioMensual(Model model) {
		List<Tienda> listaTiendas = tiendaRepository.findCurrentUserTiendas();
		model.addAttribute("listaTiendas", listaTiendas);
		model.addAttribute("mensual", new Mensual());
		return "form_mensual";
	}

	@RequestMapping(value = "mensual/guardar", method = RequestMethod.POST)
	public String guardarMensual(@ModelAttribute("mensual") Mensual mensual) {
		mensualRepository.save(mensual);
		return "redirect:/mensual";
	}

	@RequestMapping("/mensual/editar/{id}")
	public String mostrarFormularioModificarMensual(@PathVariable("id") Integer id, Model model) {
		Mensual mensual = mensualRepository.findById(id).get();
		model.addAttribute("mensual", mensual);
		List<Tienda> listaTiendas = tiendaRepository.findCurrentUserTiendas();
		model.addAttribute("listaTiendas", listaTiendas);
		return "form_mensual";
	}

	// Eliminación de reg.mensual

	/*
	 * @RequestMapping("/mensual/eliminar/{id}") public String
	 * eliminarMensual(@PathVariable("id") Integer id) {
	 * mensualRepository.deleteById(id); return "redirect:/mensual"; }
	 */
}
