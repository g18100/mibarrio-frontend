package com.mibarrio_web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin("*")
public class NavController {

	@RequestMapping("/login")
	public String iniciarSesion() {
		return "login";
	}
}
