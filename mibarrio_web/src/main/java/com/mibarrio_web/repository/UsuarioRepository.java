package com.mibarrio_web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mibarrio_web.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	Usuario findUserByEmail(String email);
	
	@Query("select u from Usuario u where u.username = ?#{ principal?.username }")
	List<Usuario> findCurrentUser();
}
