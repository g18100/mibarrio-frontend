package com.mibarrio_web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mibarrio_web.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer>{

	@Query("select p from Producto p inner join p.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } order by p.id desc")
	List<Producto> findCurrentUserProductos();
	
	@Query("select p from Producto p inner join p.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } and concat(p.fechaVenta, p.nombreProducto, p.referenciaProducto) like %?1% order by p.id desc")
	List<Producto> findCurrentUserProductosSearch(String name);
	
	@Query("select p from Producto p inner join p.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } and (p.perecedero = 1 or p.fechaCaducidad != '') and p.precioVenta = 0 and p.baja = 0 order by p.fechaCaducidad")
	List<Producto> findCurrentUserProductosPerecedero();
	
	@Query("select p from Producto p inner join p.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } and p.precioVenta = 0 and p.baja = 0 order by p.id")
	List<Producto> findCurrentUserProductosStock();
	
	@Query("select p from Producto p inner join p.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } and p.precioVenta != 0 order by p.fechaVenta desc")
	List<Producto> findCurrentUserProductosVentas();
}
