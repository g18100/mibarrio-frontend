package com.mibarrio_web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mibarrio_web.model.Mensual;

public interface MensualRepository extends JpaRepository<Mensual, Integer>{
	@Query("select m from Mensual m inner join m.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } order by m.id desc")
	List<Mensual> findCurrentUserMensual();
	
	@Query("select m from Mensual m inner join m.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } and concat(m.fechaPagoServicio, m.nombreServicio) like %?1% order by m.id desc")
	List<Mensual> findCurrentUserMensualSearch(String name);
	
	@Query("select m from Mensual m inner join m.tienda t inner join t.usuario u where u.username = ?#{ principal?.username } order by m.fechaPagoServicio desc")
	List<Mensual> findCurrentUserMensualFecha();
}
