package com.mibarrio_web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mibarrio_web.model.Tienda;

public interface TiendaRepository extends JpaRepository<Tienda, Integer>{
	
	@Query("select t from Tienda t inner join t.usuario u where u.username = ?#{ principal?.username }")
	List<Tienda> findCurrentUserTiendas();
}
