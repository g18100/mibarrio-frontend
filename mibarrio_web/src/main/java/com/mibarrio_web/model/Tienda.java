package com.mibarrio_web.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "tienda")
public class Tienda {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	@JsonProperty(access = Access.WRITE_ONLY)
	private Usuario usuario;

	@OneToMany(mappedBy = "tienda", cascade = CascadeType.ALL)
	private List<Producto> productos;

	@OneToMany(mappedBy = "tienda", cascade = CascadeType.ALL)
	private List<Mensual> mensual;

	@Column(name = "registro_comercial", length = 255, nullable = false)
	private String registroComercial;

	@Column(name = "nombre_tienda", length = 125, nullable = false)
	private String nombreTienda;

	@Column(name = "barrio", length = 45)
	private String barrio;

	@Column(name = "zona", length = 45)
	private String zona;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public List<Mensual> getMensual() {
		return mensual;
	}

	public void setMensual(List<Mensual> mensual) {
		this.mensual = mensual;
	}

	public String getRegistroComercial() {
		return registroComercial;
	}

	public void setRegistroComercial(String registroComercial) {
		this.registroComercial = registroComercial;
	}

	public String getNombreTienda() {
		return nombreTienda;
	}

	public void setNombreTienda(String nombreTienda) {
		this.nombreTienda = nombreTienda;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

}
