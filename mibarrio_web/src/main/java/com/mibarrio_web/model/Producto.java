package com.mibarrio_web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "producto")
public class Producto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "tienda_id", nullable = false)
	@JsonProperty(access = Access.WRITE_ONLY)
	private Tienda tienda;

	@Column(name = "referencia_producto", length = 45)
	private String referenciaProducto;

	@Column(name = "codigo_proveedor", length = 45)
	private String codigoProveedor;

	@Column(name = "nombre_producto", length = 255)
	private String nombreProducto;

	@Column(name = "costo_adquisicion")
	private double costoAdquisicion;

	@Column(name = "perecedero")
	private boolean perecedero;

	@Column(name = "fecha_caducidad", length = 12)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private String fechaCaducidad;

	@Column(name = "baja")
	private boolean baja;

	@Column(name = "precio_venta")
	private double precioVenta;

	@Column(name = "fecha_venta", length = 12)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private String fechaVenta;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Tienda getTienda() {
		return tienda;
	}

	public void setTienda(Tienda tienda) {
		this.tienda = tienda;
	}

	public String getReferenciaProducto() {
		return referenciaProducto;
	}

	public void setReferenciaProducto(String referenciaProducto) {
		this.referenciaProducto = referenciaProducto;
	}

	public String getCodigoProveedor() {
		return codigoProveedor;
	}

	public void setCodigoProveedor(String codigoProveedor) {
		this.codigoProveedor = codigoProveedor;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public double getCostoAdquisicion() {
		return costoAdquisicion;
	}

	public void setCostoAdquisicion(double costoAdquisicion) {
		this.costoAdquisicion = costoAdquisicion;
	}

	public boolean isPerecedero() {
		return perecedero;
	}

	public void setPerecedero(boolean perecedero) {
		this.perecedero = perecedero;
	}

	public String getFechaCaducidad() {
		return fechaCaducidad;
	}

	public void setFechaCaducidad(String fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public boolean isBaja() {
		return baja;
	}

	public void setBaja(boolean baja) {
		this.baja = baja;
	}

	public double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public String getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(String fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public double getIngresoVenta() {
		if (getPrecioVenta() != 0) {
			return getPrecioVenta() - getCostoAdquisicion();
		} else {
			return 0;
		}
	}
}
