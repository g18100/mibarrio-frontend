package com.mibarrio_web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "mensual")
public class Mensual {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "tienda_id", nullable = false)
	@JsonProperty(access = Access.WRITE_ONLY)
	private Tienda tienda;

	@Column(name = "nombre_servicio", length = 45)
	private String nombreServicio;

	@Column(name = "costo_servicio")
	private double costoServicio;

	@Column(name = "fecha_pago_servicio", length = 12)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private String fechaPagoServicio;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Tienda getTienda() {
		return tienda;
	}

	public void setTienda(Tienda tienda) {
		this.tienda = tienda;
	}

	public String getNombreServicio() {
		return nombreServicio;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public double getCostoServicio() {
		return costoServicio;
	}

	public void setCostoServicio(double costoServicio) {
		this.costoServicio = costoServicio;
	}

	public String getFechaPagoServicio() {
		return fechaPagoServicio;
	}

	public void setFechaPagoServicio(String fechaPagoServicio) {
		this.fechaPagoServicio = fechaPagoServicio;
	}

}
